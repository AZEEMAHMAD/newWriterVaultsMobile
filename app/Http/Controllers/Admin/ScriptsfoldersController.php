<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Scriptsfolder;
use Illuminate\Http\Request;
use App\Authorizable;
use Illuminate\Support\Facades\File;

class ScriptsfoldersController extends Controller
{
     use Authorizable;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $scriptsfolders = Scriptsfolder::where('name', 'LIKE', "%$keyword%")
                ->orWhere('status', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $scriptsfolders = Scriptsfolder::latest()->paginate($perPage);
        }

        return view('admin.scriptsfolders.index', compact('scriptsfolders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.scriptsfolders.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'name' => 'required'
		]);
        $requestData = $request->all();
        File::makeDirectory('UserScripts/'.$requestData['name'],775, true);
        Scriptsfolder::create($requestData);

        return redirect('admin/scriptsfolders')->with('flash_message', 'Scriptsfolder added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $scriptsfolder = Scriptsfolder::findOrFail($id);

        return view('admin.scriptsfolders.show', compact('scriptsfolder'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $scriptsfolder = Scriptsfolder::findOrFail($id);

        return view('admin.scriptsfolders.edit', compact('scriptsfolder'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'name' => 'required'
		]);
        $requestData = $request->all();
        
        $scriptsfolder = Scriptsfolder::findOrFail($id);
        rename("UserScripts/$scriptsfolder->name", "UserScripts/".$requestData['name']);
        $scriptsfolder->update($requestData);

        return redirect('admin/scriptsfolders')->with('flash_message', 'Scriptsfolder updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $scriptsfolder = Scriptsfolder::findOrFail($id);
        File::deleteDirectory(public_path('UserScripts/'.$scriptsfolder ->name));
        Scriptsfolder::destroy($id);
        return redirect('admin/scriptsfolders')->with('flash_message', 'Scriptsfolder deleted!');
    }
}
