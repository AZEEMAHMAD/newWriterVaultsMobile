@extends('client.layouts.master')
<style>
    .help-block{
        color: red;
    }
</style>
@section('content')
    <main id="main">
        <section id="why-us" class="outer">
            <div class="container-fluid">
             <div class="row">
                    <div class="col-lg-6 no-padding">
                        <div class="why-us-img">
                            <img src="https://dummyimage.com/900x900/cccccc/000000" alt="" class="img-fluid">
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="row">
                            <div class="signup-form-outer">
                                <h2 class="main-title">WRITERS VAULT</h2>
                                <h5 class="sub-title">Please complete to create your account.</h5>
                                <div class="col-md-8 offset-2">
                                    <form id="msform" action="{{url('/client/paytmpayment')}}" method="get">
                                        <!-- progressbar -->
                                        <ul id="progressbar">
                                            <li class="active">Details</li>
                                            <li>Payment Info</li>
                                        </ul>
                                        <fieldset>
                                            <input type="submit" name="previous" class="action-button-previous" value="Payment"/>
                                        </fieldset>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="form-footer col-lg-6 offset-3">
                            <div class=""><a href="{{ url('/terms') }}">Terms of use</a></div>
                            <div class=""><a href="{{ url('/privacy') }}">Privacy policy</a></div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection