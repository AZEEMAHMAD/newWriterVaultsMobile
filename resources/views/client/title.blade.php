@extends('client.layouts.master')
<style>
.help-block {
  color: red;
}

</style>
@section('content')
  <main id="main">

    <!--==========================
    Why Us Section
    ============================-->
    <section id="why-us" class="outer custom-outer">

      @include('client.layouts.sub_header')

      <div class="container">
        <div class="row padding-tb">

          @include('client.layouts.side_menu')

          <div class="col-lg-9 custom-right-block-outer">

            <!-- MultiStep Form -->
            <div class="row padding-lr-15">
              <div class="custom-right-block">

                <div class="row">
                  <div class="col-lg-12 display-flex">
                    <div class="col-lg-9">
                      <h4 class="right-title">Titles {{ $file_type }}</h4>
                      <h5 class="rsub-title">All your works/drafts around one title.</h5>
                    </div>
                  </div>
                </div>

                <div class="divider"></div>

                <div id="content" class="tab-content" role="tablist">
                  <div id="registration" class="card tab-pane fade show active" role="tabpanel"
                  aria-labelledby="registration">

                  <div id="collapse-A" class="collapse show" role="tabpanel"
                  aria-labelledby="heading-A">
                  <div class="card-body">
                    <div class="container no-padding">

                      <div class="col-md-12 float-left folder-block">
                        <div class="container">
                          @if($files->isNotEmpty())
                            @foreach($files as $name)
                              <div class="col-md-2 float-left folderopen" data-id="{{$name->id}}">
                                <a href="{{ url('client/files') }}/{{ urlencode($name->title) }}">
                                  <img src="{{asset('img/folder-icon.png')}}">
                                  <p>{{ucwords($name->title)}}</p>
                                </a>
                              </div>
                            @endforeach
                            @else
                            <h6>No Files</h6>
                          @endif
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

            </div>
          </div>
        </div>

      </div>
    </div>
  </div>
  <div class="container">
    <div class="form-footer1 col-lg-3 offset-9">
      <div class=""><a href="{{ url('/terms') }}">Terms of use</a></div>
      <div class=""><a href="{{ url('/privacy') }}">Privacy policy</a></div>
    </div>
  </div>
</section>
</main>


@endsection
@section('scripts')
  <script>
  $('#tabs').find('.title').addClass('active');
  </script>
@endsection
