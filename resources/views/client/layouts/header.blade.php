<header id="header">
    <div class="container">
        <div class="logo float-left">
            <h1 class="text-light"><a href="{{url('/')}}" class="scrollto"><img src="{{asset('img/logo.png')}}"></a></h1>
        </div>
        <nav class="main-nav float-right d-none d-lg-block">
            <ul>
                {{--<li><a href="#">Future</a></li>--}}


                @if(Auth::guard('client')->guest())
                    <li><a href="#">About</a></li>
                    <li><a href="#">Contact Us</a></li>
                  <li><a href="{{url('client/login')}}">Login</a></li>
                  <li><a href="{{url('client/register')}}">Register</a></li>
                @endif
            </ul>
        </nav>
    </div>
</header>
