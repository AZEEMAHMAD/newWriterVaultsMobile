<div class="container-fluid page-header-outer">
    <div class="container no-padding">
        <div class="page-header col-lg-6">
            <div class="bcircle"><img src="{{url('img/icons/f.png')}}"/></div><span class="ltitle">FAQs</span>
        </div>

        <div class="page-header right-block col-lg-5 offset-1">
            <div class="col-md-12 account-nav">
                <nav class="main-nav float-right d-none d-lg-block no-padding">
                    <ul class="no-padding">
                        <li class="drop-down"><a href=""><i class="fa fa-user-circle-o" aria-hidden="true"></i>{{Auth::guard('client')->user()->user_name}}</a>
                            <ul>
                                <li><a href="{{url('/client/paytmpayment')}}">Add Credits</a></li>
                                <li><a href="{{url('/client/changepassword')}}">Change Password</a></li>
                                <li><a href="#">Feedback</a></li>
                                <li><a href="{{url('/client/logout')}}">Logout</a></li>
                            </ul>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>

</div>
