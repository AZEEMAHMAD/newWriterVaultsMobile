@extends('client.layouts.master')
<style>
    .help-block{
        color: red;
    }
</style>
@section('content')
  <main id="main">

    <section id="faq">
      <div class="container">
        <header class="section-header">
          <h3>Frequently Asked Questions</h3>
          <h4><strong>General</strong></h4>
        </header>

        <ul id="faq-list" class="wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;">
          <li>
            <a data-toggle="collapse" class="collapsed" href="#gen1">What is WritersVault.io?<i class="ion-android-remove"></i></a>
            <div id="gen1" class="collapse" data-parent="#faq-list">
              <p>
                WritersVault.io is a digital rights registration, management and verification platform for digital text works powered by Blockchain technology
              </p>
            </div>
          </li>

          <li>
            <a data-toggle="collapse" href="#gen2" class="collapsed">When should I register my work on WritersVault.io?<i class="ion-android-remove"></i></a>
            <div id="gen2" class="collapse" data-parent="#faq-list">
              <p>
                It is recommended that you register your work as soon as possible after you have created it. It is also recommended that you register your work prior to publishing, sharing or any private/public exposure. Your rights are strengthened when the time of registration is as close as possible to the time of creation
              </p>
            </div>
          </li>

          <li>
            <a data-toggle="collapse" href="#gen3" class="collapsed">What digital works can I register on WritersVault.io?<i class="ion-android-remove"></i></a>
            <div id="gen3" class="collapse" data-parent="#faq-list">
              <p>
                Handwritten or scanned images are <strong>NOT</strong> advisable. All textual digital works in a PDF file format can be registered. Your material needs to be typed and be in a pdf format. The PDF can be in any Indian language.  You can submit the following - Logline, Synopsis, Treatment, Screenplay, Shoot Script and Lyrics
              </p>
            </div>
          </li>

          <li>
            <a data-toggle="collapse" href="#gen4" class="collapsed">Is there a file size limit?<i class="ion-android-remove"></i></a>
            <div id="gen4" class="collapse" data-parent="#faq-list">
              <p>
                No. There is no limit to the file size on WritersVault.io. The only limitation is the processing power of your computer.
              </p>
            </div>
          </li>

          <li>
            <a data-toggle="collapse" href="#gen5" class="collapsed">How many files can I register/verify at the same time?<i class="ion-android-remove"></i></a>
            <div id="gen5" class="collapse" data-parent="#faq-list">
              <p>
                You can register up to a maximum of 1 file at any one time. The speed of registration/verification is limited by the processing power of your computer and your internet bandwidth.
              </p>
              <p><strong>UPLOADING THE RIGHT PDF FILE:</strong></p>
              <ul>
                <li>Acceptable Font size is 12-14 (standard single spacing).</li>
                <li>DO NOT upload PDFs having scanned images. Use only PDFs of text documents.</li>
                <li>The PDF file must be A4 size with portrait orientation (landscape not accepted).</li>
                <li>There must be a margin of 1 inch on top and bottom of every page of your work.</li>
                <li>DO NOT protect your file using any password. Password protected files will not be processed by system.</li>
                <li>DO NOT include any Header or Footer in your file. They will be inserted by the system as proof of registration.</li>
              </ul>
            </div>
          </li>

          <li>
            <a data-toggle="collapse" href="#gen6" class="collapsed">Can I register a file with a duplicate filename?<i class="ion-android-remove"></i></a>
            <div id="gen6" class="collapse" data-parent="#faq-list">
              <p>
                No, you cannot. You will need to rename your file before registering it. The system will alert you if you are attempting to register a file with a name that has been previously registered.
              </p>
            </div>
          </li>


          <li>
            <a data-toggle="collapse" href="#gen7" class="collapsed">Do I have to pay to setup an account with WritersVault.io?<i class="ion-android-remove"></i></a>
            <div id="gen7" class="collapse" data-parent="#faq-list">
              <ul>
                <li>Yes. There is a membership fee of Rs. 2500 + GST</li>
                <li>Registering your script entails the following charges: - </li>
                <li>The first page of Logline, Synopsis, Treatment, Screenplay, Shoot Script, Lyrics - INR 40/-</li>
                <li>After first page - INR 2/- per page</li>
                <li>Innovation Fun - INR 5/- per transaction</li>
              </ul>
            </div>
          </li>

          <header class="section-header">
            <h4><br><strong>Dashboard</strong></h4>
          </header>

          <li>
            <a data-toggle="collapse" class="collapsed" href="#dash1">I have forgotten to download the document after registration. How will i get the registered copy?<i class="ion-android-remove"></i></a>
            <div id="dash1" class="collapse" data-parent="#faq-list">
              <p>
                An email is sent to your registered email ID after every registration, carrying the registered PDF as an attachment. However, it may take a few minutes for this email to come to you. If you don’t receive the email even after a few hours, contact Technical Support.
              </p>
            </div>
          </li>

          <li>
            <a data-toggle="collapse" href="#dash2" class="collapsed">I get errors while registration. why? or my account was debited but registration hasn’t taken place. why?<i class="ion-android-remove"></i></a>
            <div id="dash2" class="collapse" data-parent="#faq-list">
              <p>
              May be, your PDF file had a compression ratio that is not compatible with our online system. Try again with a different PDF. If the money was deducted but the script was not registered then please contact Technical Support giving you transaction number. We will cancel your transaction and money will be credited to your account. Online free PDF convertors and free screenwriting softwares like CeltX can cause this problem. Use MS Word (or Mac Word), which have standard compression ratios, to convert your files to PDF.
              </p>
            </div>
          </li>

          <li>
            <a data-toggle="collapse" href="#dash3" class="collapsed">Why is “REGISTER YOUR WORK” tab inactive on my dashboard?<i class="ion-android-remove"></i></a>
            <div id="dash3" class="collapse" data-parent="#faq-list">
              <p>
                Check your Membership Validity on the membership card. It might need a renewal.
              </p>
            </div>
          </li>


          <header class="section-header">
            <h4><br><strong>Security</strong></h4>
          </header>

          <li>
            <a data-toggle="collapse" class="collapsed" href="#sec1">HOW DOES writersvault.io’s ONLINE SCRIPT REGISTRATION WORK?<i class="ion-android-remove"></i></a>
            <div id="sec1" class="collapse" data-parent="#faq-list">
              <p>
                A. The Online Script Registration system asks the user to upload a PDF and counts the number of pages. Once the payment is confirmed, it generates a registered PDF with a WritersVault.io registration number and unique digital ID, stamped in a QR code. The QR Code retains the registration details. The user is shown a temporary link through which he or she can download the registered soft copy. Additionally, one can also get an email at the registered email ID carrying the registered soft copy as an attachment.
              </p>
              <p>The following are the TECHNICAL INSTRUCTIONS for Online Script Registration:</p>
              <ul>
                <li>Any attempt to tamper with the document will invalidate the digital signature and cancel the registration.</li>
                <li>For secrecy and security purposes, WritersVault.io website DOES NOT save your files/work. Therefore, only you are responsible for the safekeeping of your Registered Files.</li>
                <li>You can register a maximum of FOUR Articles per transaction by uploading FOUR PDFS. However, registration of multiple Article/Songs/Mukhda in a single PDF will lead to cancellation of the registration.</li>
                <li>You can also add a maximum of TWO Co-authors per transaction. However, due to technical limitations, while adding Co-author/s you won't be able to add multiple PDFs.</li>
                <li>DO NOT proceed with payment if the number of pages is shown incorrect. If there is a discrepancy in the number of actual pages and the payment calculation, it will lead to cancellation of the registration.</li>
                <li>Avoid apostrophes and special characters in the script title.</li>
              </ul>
            </div>
          </li>

          <li>
            <a data-toggle="collapse" href="#sec2" class="collapsed">Is it safe to share my work professionally after registering it with writersvault.io?<i class="ion-android-remove"></i></a>
            <div id="sec2" class="collapse" data-parent="#faq-list">
              <p>
              Yes. WritersVault.io’s Script Registration process is recognized and abided by the film industry as well as the entire body of film associations. However, it’s advisable to share only the registered softcopies (or the Xerox of the registered copies) as a deterrent to any Copyright infringement.
              </p>
            </div>
          </li>

          <li>
            <a data-toggle="collapse" href="#sec3" class="collapsed">I am concerned with privacy. Do you keep a copy of my file? <i class="ion-android-remove"></i></a>
            <div id="sec3" class="collapse" data-parent="#faq-list">
              <p>
                No. Your file is not transmitted to us. The file is processed on your computer and only a unique cryptographic snapshot called a hash is registered on the Blockchain.
              </p>
            </div>
          </li>

        </ul>

      </div>



    </section>



  </main>
@endsection
@section('scripts')
<script type="text/javascript">
$('.custom-carousel').owlCarousel({
    autoplay: true,
    center: true,
    loop: true,
    nav: false
  });
</script>
@endsection
