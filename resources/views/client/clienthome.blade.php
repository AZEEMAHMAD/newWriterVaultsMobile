@extends('client.layouts.master')
<style>
.help-block {
  color: red;
}

</style>
@section('content')
  <main id="main">

    <!--==========================
    Why Us Section
    ============================-->
    <section id="why-us" class="outer custom-outer">

      @include('client.layouts.sub_header')

        <div class="container">
          <div class="row padding-tb">

              @include('client.layouts.side_menu')

                        <div class="col-lg-9 custom-right-block-outer">

                          <!-- MultiStep Form -->
                          <div class="row padding-lr-15">
                            <div class="custom-right-block">

                              <div class="row">
                                <div class="col-lg-12 col-md-12 display-flex">
                                  <div class="col-lg-8 col-md-8">
                                    <h4 class="right-title">Registration</h4>
                                    <h5 class="rsub-title">Register your files for copyright.</h5>
                                  </div>

                                  <div class="col-lg-4 col-md-4">
                                    <!-- <a class="add-file" href="#"><i class="fa fa-plus-circle" aria-hidden="true"></i>Add Another File</a> -->
                                    <div class="avatar-upload add-file-outer float-right">
                                      <div class="avatar-edit add-file">
                                        {{--<input type='file' id="imageUpload" accept=".png, .jpg, .jpeg" />--}}
                                        <label for="imageUpload"><a class=""
                                          href="#onlinescriptregistration"
                                          data-toggle="modal"><span class=""><i
                                            class="fa fa-plus-circle"
                                            aria-hidden="true"></i> Add Another File</span></a></label>

                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>

                                  <div class="divider"></div>

                                  <div id="content" class="tab-content" role="tablist">
                                    <div id="registration" class="card tab-pane fade show active" role="tabpanel"
                                    aria-labelledby="registration">

                                    <div id="collapse-A" class="collapse show" role="tabpanel"
                                    aria-labelledby="heading-A">
                                    <div class="card-body">
                                      <div class="container no-padding">

                                        <div class="col-md-12 float-left registration-upload-outer">

                                          <div class="registration-upload text-center">
                                            <!-- <h4>Credits Required</h4> -->
                                            <p>

                                              <img src="{{asset('img/upload-icon.png')}}" class="">

                                              <!-- <span class="bottom-span">Total cost 'y' credits.</span> -->
                                            </p>
                                            <!-- <a class="update-kyc" href="#updatekyc" data-toggle="modal"><span class="ltitle">Update KYC</span></a> -->
                                            <div>
                                              <a class=""
                                              href="#onlinescriptregistration"
                                              data-toggle="modal"><button type="submit" class="submit-btn"> Upload
                                              </button></a>
                                            </div>
                                            <br><span class="">Each file require 'x' credits to register.</span>

                                          </div>

                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                              </div>
                            </div>
                          </div>

                        </div>
                      </div>
                    </div>

                    <div class="container">
                      <div class="form-footer1 col-lg-3 offset-9">
                        <div class=""><a href="{{ url('/terms') }}">Terms of use</a></div>
                        <div class=""><a href="{{ url('/privacy') }}">Privacy policy</a></div>
                      </div>
                    </div>
                  </section>
                </main>

                <div id="onlinescriptregistration" class="modal onlinescriptregistration" data-easein="expandIn" tabindex="-1"
                role="dialog" aria-labelledby="costumModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                  <div class="modal-content">
                    <div class="modal-header onlineheader">
                      <h5 class="">Online Script Registration Form</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        ×
                      </button>
                    </div>
                    <div class="modal-body text-center">
                      <div class="container no-padding">
                        <div class="">
                          <form class="form-horizontal contactForm" role="form" action="JavaScript:void(0)" id='clientRegistrationform'
                          method="POST" enctype="multipart/form-data">
                          {{csrf_field()}}
                          <div id="conterrormsg341"></div>
                          <div id="conterrormsg143"></div>
                            <input type="hidden" id="initialCost" value="{{$cost_data}}">
                          <div class="col-md-6 no-padding float-left">
                            <div class="form-group">
                              <label for="name" class="label">Author</label>
                              <input type="hidden" name="client_id" value="{{$client->id}}">
                              <input type="text" name="auther_name" class="form-control" id="auther_name" placeholder="Writer's Name" value="{{ Auth::guard('client')->user()->first_name }} {{ Auth::guard('client')->user()->last_name }}" data-msg="Enter Writer's Name" readonly/>

                              <div class="validation"></div>
                            </div>
                            <div class="form-group">
                              <label for="file_type" class="label">File</label>
                              <div class="myselect">
                                <select name="file_type" id="file_type">
                                  <option selected value="">Type of Creation</option>
                                  @foreach(App\Models\Scriptsfolder::orderBy('name')->where('status',1)->get() as $name)
                                    <option value="{{$name->name}}">{{ucwords($name->name)}}</option>
                                  @endforeach
                                </select>
                              </div>
                              <div class="validation"></div>
                            </div>

                            <div class="form-group">
                              <div class="titleinput">
                                <input type="text" name="title" class="form-control" id="title"
                                placeholder="Title" data-msg="Enter Title"/>
                              </div>
                              <div class="validation"></div>
                            </div>

                            <div class="form-group">
                              <label for="name" class="label">Upload PDF</label><br>

                              <div class="upload-file-outer">
                                <input type="file" id="file1" name="file1" multiple/>
                                <i class="fa fa-remove remove customremove" style="font-size:36px"></i>
                              </div>


                              <div class="validation"></div>
                            </div>
                            <div class="form-group pull-right" id="displayfees" style="display: none">
                              <div><span style="color: red">Total Amount is:</span> <span style="color: green" id="dispalytotalamount"></span></div>
                            </div>
                          </div>

                          <div class="col-md-6 no-padding float-left">
                            <div class="form-group">
                              <label for="co_auther_number" class="label">Co-Author if any</label>
                              <input type="text" name="co_auther_number" class="form-control" id="co_auther_number"
                              placeholder="Co-Author's Name" data-msg="Enter Co-Author's Name"/>

                              <div class="validation"></div>
                            </div>

                            <div style="display:none" class="form-group margin-btn-1em">
                              <label for="name" class="label more-pdf">You can add two more pdfs</label><br>

                              <div class="upload-file-outer">
                                <input type="file" name="file2" id="file2" multiple/>
                                <i class="fa fa-remove remove1 customremove" style="font-size:36px"></i>
                              </div>
                              <div class="validation"></div>
                            </div>

                            <div  style="display:none" class="form-group margin-btn-1em">
                              <div class="upload-file-outer">
                                <input type="file" name="file3" id="file3" multiple/>
                                <i class="fa fa-remove remove2 customremove" style="font-size:36px"></i>
                              </div>
                              <div class="validation"></div>
                            </div>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                  <div class="modal-footer">
                    <div class="">
                      <div class="checkbox">
                        <label class="label">Declaration by the Member</label>
                        <label class="mylabel"><input type="checkbox" name="declartion" id="declartion" required>&nbsp;&nbsp; I accept that</label>
                      </div>
                      <p>1. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit
                        anim id est eopksio laborum. Sed ut perspiciatis unde omnis istpoe natus error sit
                        voluptatem accusantium doloremque eopsloi laudantium.</p>

                        <p>2. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit
                          anim id est eopksio laborum. Sed ut perspiciatis unde omnis istpoe natus error sit
                          voluptatem accusantium doloremque eopsloi laudantium.</p>

                          <button type="submit" class="submit-btn" id="registerartion_form_submit"> Submit</button>
                          <button type="button" class="submit-btn hide" id="rzp-button1" style="display:none">Make Payment</button>
                          <button class="btn btn-default btn-close" data-dismiss="modal" aria-hidden="true">
                            Close
                          </button>
                          <span id="file_msg"></span>
                        </div>


                      </div>
                    </div>
                  </div>
                </div>

                {{-- <div id="requirecredits" class="modal" data-easein="expandIn"  tabindex="-1" role="dialog" aria-labelledby="costumModalLabel" aria-hidden="true">
                  <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                          ×
                        </button>
                      </div>
                      <div class="modal-body text-center">
                        <h4>Credits Required</h4>
                        <p>

                          <img src="{{asset('img/dummy.jpg')}}" class="illustrationimg">
                          <span class="">Each file require 'x' credits to register.</span><br>
                          <span class="bottom-span">Total cost 'y' credits.</span>
                        </p>
                        <!-- <a class="update-kyc" href="#updatekyc" data-toggle="modal"><span class="ltitle">Update KYC</span></a> -->
                        <div>
                          <button type="submit" class="submit-btn"><div class="scirclekyc"></div> Proceed
                          </button>
                        </div>
                        <button class="btn btn-default btn-close" data-dismiss="modal" aria-hidden="true">
                          Back
                        </button>
                      </div>
                      <div class="modal-footer">
                      </div>
                    </div>
                  </div>
                </div> --}}


              @endsection
              @section('scripts')
                <script src="https://checkout.razorpay.com/v1/checkout.js"></script>
                <script>
                $('#tabs').find('.registration').addClass('active');
                var options;
                var rzp1;
                var host_url = location.protocol + "//" + location.host;
                $('#registerartion_form_submit').click(function(){
                  var authorName = $('#auther_name').val();
                  var file_type= $('#file_type').val();
                  var title=$('#title').val();
                  var file1=$('#file1').val();
                  var co_auther_number = $('#co_auther_number').val();
                  var file2=$('#file2').val();
                  var file3=$('#file3').val();
                  var allowedExtensions = /(\.pdf)$/i;

                  if (authorName == '') {
                    alert('Enter Author name !');
                    return false;
                  }
                  else if (file_type == '') {
                    alert('Select File Type of creation');
                    return false;
                  }
                  else if (title == '') {
                    alert('Enter Title !');
                    return false;
                  }
                  else if (file1 == '') {
                    alert('Upload File');
                    return false;
                  }
                  else if($('#declartion').is(":not(:checked)")){
                    alert("Please accept the declaration.");
                    return false;
                  }
                  if (!allowedExtensions.exec(file1)) {
                    alert('Please upload only pdf file');
                    file1 = '';
                    return false;
                  }
                  if(file2 != ''){
                    if (!allowedExtensions.exec(file2)) {
                      alert('Please upload only pdf file');
                      $('#file2').val();
                      return false;
                    }
                  }
                  if(file3 != ''){
                    if (!allowedExtensions.exec(file3)) {
                      alert('Please upload only pdf file');
                      $('#file3').val();
                      return false;
                    }
                  }



                  $('form#clientRegistrationform').submit();
                  var form = $('form#clientRegistrationform')[0];
                  var formData = new FormData(form);
                  $.ajax({
                    headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: "/client/scriptsave",
                    type: 'POST',
                    data: formData,//new FormData('this'), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
                    contentType: false,       // The content type used when sending data to the server.
                    cache: false,             // To unable request pages to be cached
                    processData: false,
                    success: function (response) {

                      var res = JSON.parse(response);

                      if(res.status == 1)
                      {
                        $('#registerartion_form_submit').css('display','none');
                        $('#rzp-button1').css('display','block');
                        $('#clientRegistrationform').trigger('reset');

                        $('#rzp-button1').removeClass('hidden');
                        options = {
                          "key": "{{ env('RAZOR_PAY_KEY_ID') }}", // Enter the Key ID generated from the Dashboard
                          "name": "Writer Valts",
                          "description": "",
                          "image": host_url+"/img/logo.png",
                          "order_id": res.razorpay_order_id,
                          "handler": function (response){
                            // alert(response.razorpay_payment_id);
                            // console.log(response);
                            $.ajax({
                              url: "/client/saverazorpayresponse",
                              type: 'POST',
                              data: response,//new FormData('this'), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
                              success: function (response) {
                                if(response==1)
                                {
                                  window.location.href="history";
                                }
                              }
                            });
                          },
                          "prefill": {
                          },
                          "notes": {
                            "address": "note value"
                          },
                          "theme": {
                            "color": "#F37254"
                          }
                        };
                        rzp1 = new Razorpay(options);

                      }
                      else if (response == 2) {
                        $(this).removeAttr('disabled');
                        $('#conterrormsg143').html('Your scripts Not uploaded Successfully !');
                        $('#conterrormsg143').css('color', 'red');
                        $('#registerartion_form_submit').val("Submit");
                        $('#registerartion_form_submit').prop('disabled', false);
                        document.getElementById("clientRegistrationform").reset();
                        //                        $('#conterrormsg341').html('Your scripts uploaded successfully.');
                        //                        $('#conterrormsg341').css('color', 'green');
                        //                        $("#registerartion_form_submit").val("Submit");
                        //                        $('#registerartion_form_submit').prop('disabled', false);
                        //                        document.getElementById("clientRegistrationform").reset();
                        //                        setTimeout(function () {
                        //                            location.reload();
                        //                        }, 5000);

                      }
                      else {
                        $( ".btn-close" ).trigger( "click" );
                        $('.card-body').html(response);


                        //                        $(this).removeAttr('disabled');
                        //                        $('#conterrormsg143').html('Your scripts Not uploaded Successfully !');
                        //                        $('#conterrormsg143').css('color', 'red');
                        //                        $('#registerartion_form_submit').val("Submit");
                        //                        $('#registerartion_form_submit').prop('disabled', false);
                        //                        document.getElementById("clientRegistrationform").reset();

                      }
                    }
                  })
                });

                $(".folderopen").dblclick(function(){
                  alert("The paragraph was double-clicked");
                });

                var initialprice =$('#initialCost').val()

                $('#file1').change(function(){
                  var file1= $('#file1').val();
                  var file2= $('#file2').val();
                  var file3= $('#file3').val();
                  var allowedExtensions = /(\.pdf)$/i;
                  if (!allowedExtensions.exec(file1)) {
                    alert('Please upload only pdf file');
                    $('#file1').val('');
                    file1='';
                    $('#displayfees').show();
                    if(file2 =='' && file3==''){
                      $('#dispalytotalamount').html('Rs.0');
                    }
                    else if(file2 !='' && file3==''){
                      $('#dispalytotalamount').html('Rs.'+2*initialprice);
                    }
                    else if(file2 =='' && file3 !=''){
                      $('#dispalytotalamount').html('Rs.'+1*initialprice);
                    }
                    else if(file2 !='' && file3 !=''){
                      $('#dispalytotalamount').html('Rs.'+2*initialprice);
                    }
                    return false;
                  }
                  else{
                    if(file1 != '' || file1 != null){
                      $('#displayfees').show();
                      if(file2 =='' && file3==''){
                        $('#dispalytotalamount').html('Rs.'+initialprice);
                      }
                      else if(file2 !='' && file3==''){
                        $('#dispalytotalamount').html('Rs.'+2*initialprice);
                      }
                      else if(file2 =='' && file3 !=''){
                        $('#dispalytotalamount').html('Rs.'+2*initialprice);
                      }
                      else if(file2 !='' && file3 !=''){
                        $('#dispalytotalamount').html('Rs.'+3*initialprice);
                      }
                    }
                  }
                });
                $('#file2').change(function(){
                  var file1= $('#file1').val();
                  var file2= $('#file2').val();
                  var file3= $('#file3').val();
                  var allowedExtensions = /(\.pdf)$/i;
                  if (!allowedExtensions.exec(file2)) {
                    alert('Please upload only pdf file');
                    $('#file2').val('');
                    file2='';
                    $('#displayfees').show();
                    if(file1 =='' && file3==''){
                      $('#dispalytotalamount').html('Rs.0');
                    }
                    else if(file1 !='' && file3==''){
                      $('#dispalytotalamount').html('Rs.'+1*initialprice);
                    }
                    else if(file1 =='' && file3 !=''){
                      $('#dispalytotalamount').html('Rs.'+1*initialprice);
                    }
                    else if(file1 !='' && file3 !=''){
                      $('#dispalytotalamount').html('Rs.'+2*initialprice);
                    }
                    return false;
                  }
                  if(file2 != ''){
                    $('#displayfees').show();
                    if(file1 =='' && file3==''){
                      $('#dispalytotalamount').html('Rs.'+1*initialprice);
                    }
                    else if(file1 !='' && file3==''){
                      $('#dispalytotalamount').html('Rs.2'+2*initialprice);
                    }
                    else if(file1 =='' && file3 !=''){
                      $('#dispalytotalamount').html('Rs.'+2*initialprice);
                    }
                    else if(file1 !='' && file3 !=''){
                      $('#dispalytotalamount').html('Rs.'+3*initialprice);
                    }
                  }
                });
                $('#file3').change(function(){
                  var file1= $('#file1').val();
                  var file2= $('#file2').val();
                  var file3= $('#file3').val();
                  var allowedExtensions = /(\.pdf)$/i;
                  if (!allowedExtensions.exec(file3)) {
                    alert('Please upload only pdf file');
                    $('#file3').val('');
                    file3='';
                    $('#displayfees').show();
                    if(file1 =='' && file2==''){
                      $('#dispalytotalamount').html('Rs.0');
                    }
                    else if(file1 !='' && file2==''){
                      $('#dispalytotalamount').html('Rs.'+1*initialprice);
                    }
                    else if(file1 =='' && file2 !=''){
                      $('#dispalytotalamount').html('Rs.'+1*initialprice);
                    }
                    else if(file1 !='' && file2 !=''){
                      $('#dispalytotalamount').html('Rs.'+2*initialprice);
                    }
                    return false;
                  }
                  if(file3 != ''){
                    $('#displayfees').show();
                    if(file1 =='' && file2==''){
                      $('#dispalytotalamount').html('Rs.'+1*initialprice);
                    }
                    else if(file1 !='' && file2==''){
                      $('#dispalytotalamount').html('Rs.'+2*initialprice);
                    }
                    else if(file1 =='' && file2 !=''){
                      $('#dispalytotalamount').html('Rs.'+2*initialprice);
                    }
                    else if(file1 !='' && file2 !=''){
                      $('#dispalytotalamount').html('Rs.'+3*initialprice);
                    }
                  }
                });


                document.getElementById('rzp-button1').onclick = function(e){
                  rzp1.open();
                  e.preventDefault();
                }

                options.handler = function (response){
                  console.log(response.razorpay_payment_id+'  ||  '+response.razorpay_signature);
                  // document.razorpayform.submit();
                };

                </script>
              @endsection
